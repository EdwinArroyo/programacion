/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciofinal;

import db.DataBaseUtilerias;
import db.DbManager;
import java.util.ArrayList;
import java.util.Scanner;
import static javafx.application.Platform.exit;
import modelo.Empleado;

/**
 *
 * @author Alumno
 */
public class EjercicioFinal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner scan = new Scanner(System.in);
        
        DbManager dbManager = new DbManager();
        
        dbManager.dropDB();
        dbManager.crearDB();
        int opt;
        boolean sig = true;
        
        while(sig){
            showMenu();
            opt = scan.nextInt();
        
            switch(opt){
                case 1:
                    createEmpleado();
                    break;
                case 2:
                    deleteEmpleado();
                    break;
                case 3:
                    updateEmpleadoMenu();
                    break;
                case 4:
                    getSalario();
                    break;
                case 5:
                    getEmpleados();
                    break;
                case 6:
                    getSalarioPromedio();
                    break;
                default:
                    sig = false;
                    break;
            }
        }
        System.out.println("Adios!");
           
    }
    private static void showMenu() {
        System.out.println("---------Menu Principal--------------");
        System.out.println("");
        System.out.println("Elija una opcion");
        System.out.println("");
        System.out.println("1.- Crear un nuevo Empleado");
        System.out.println("2.- Borrar un Empleado");
        System.out.println("3.- Actualizar un Empleado");
        System.out.println("4.- Consultar Salario");
        System.out.println("5.- Consultar Empleados");
        System.out.println("6.- Consultar Salario Promedio");
        System.out.println("7.- Salir");
    }

    private static void createEmpleado() {
        Scanner scan = new Scanner(System.in);
        String nombre, apellido, puesto, salario;
        System.out.println("---------Crear Nuevo Empleado--------------");
        System.out.println("");
        System.out.print("Nombre: ");
        nombre = scan.nextLine();
        System.out.print("Apellido: ");
        apellido = scan.nextLine();
        System.out.print("Puesto: ");
        puesto = scan.nextLine();
        System.out.print("Salario: ");
        salario = scan.nextLine();
        System.out.println("");
        
        System.out.println("Guardando... ");
        Empleado empleado = new Empleado(nombre, apellido, puesto, Float.parseFloat(salario));
        empleado.save();
        
        System.out.println("Guardado!");
    }

    private static void deleteEmpleado() {
        Scanner scan = new Scanner(System.in);
        int id;
        System.out.println("---------Borrar Empleado--------------");
        System.out.println("");
        System.out.print("Id  del Empleado: ");
        id = scan.nextInt();
        
        Empleado empleado = new Empleado(id);
        empleado.delete();
        
        System.out.println("Borrado!");
        
    }

    private static void updateEmpleadoMenu() {
        Scanner scan = new Scanner(System.in);
        int opt;
        System.out.println("---------Borrar Empleado--------------");
        System.out.println("");
        System.out.println("1.- Nombre");
        System.out.println("2.- Salario");
        System.out.println("");
        System.out.print("Opcion: ");
        opt= scan.nextInt();
       
        switch(opt){
            case 1:
                updateEmpleadoName();
                break;
            case 2:
                updateEmpleadoSalario();
                break;
            default:
                break;
        }
    }

    private static void updateEmpleadoName() {
        Scanner scan = new Scanner(System.in);
        int id;
        String newName;
        System.out.println("---------Actualizar Nombre de Empleado--------------");
        System.out.println("");
        System.out.print("Id del Empleado: ");
        id = scan.nextInt();
        System.out.print("Nuevo Nombre: ");
        newName = scan.next();
        System.out.println("");
        
        Empleado empleado = new Empleado(id);
        empleado.updateName(newName);
        System.out.println("Empleado " + id + " Actualizado");
        System.out.println("");
        System.out.println("");
    }

    private static void updateEmpleadoSalario() {
        Scanner scan = new Scanner(System.in);
        int id;
        float newSalario;
        System.out.println("---------Actualizar Salario de Empleado--------------");
        System.out.println("");
        System.out.print("Id del Empleado: ");
        id = scan.nextInt();
        System.out.print("Nuevo Salario5"
                + ": ");
        newSalario = scan.nextFloat();
        System.out.println("");
        
        Empleado empleado = new Empleado(id);
        empleado.updateSalario(newSalario);
        System.out.println("Empleado " + empleado.getId() + " Actualizado");
        System.out.println("");
        System.out.println("");
    }

    private static void getSalario() {
        Scanner scan = new Scanner(System.in);
        int id;
        
        System.out.println("---------Consultar Salario de Empleado--------------");
        System.out.println("");
        System.out.print("Id del Empleado: ");
        id = scan.nextInt();
        
        Empleado empleado = new Empleado(id);
        empleado.getSalarioFromDB();
        
        System.out.println("El salario del Empleado "+ empleado.getId() + " es: " +empleado.getSalario());
        System.out.println("");
        System.out.println("");
    }

    private static void getEmpleados() {
        DataBaseUtilerias db = new DataBaseUtilerias();
        ArrayList<Empleado> empleados = db.getEmpleados();
        System.out.println("---------Consultar Empleados--------------");
        System.out.println("");
        for(Empleado empleado : empleados){
            System.out.println(empleado.toString());
        }
        System.out.println("");
        System.out.println("");
    }

    private static void getSalarioPromedio() {
        DataBaseUtilerias db = new DataBaseUtilerias();
        
        float prom = db.getSalarioPromedio();
        System.out.println("---------Consultar Salario Promedio--------------");
        System.out.println("");
        System.out.println("El Salario promedio es: " + prom);
        System.out.println("");
        System.out.println("");
    }

    
}

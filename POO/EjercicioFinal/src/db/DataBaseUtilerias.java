/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Empleado;

/**
 *
 * @author e28v1
 */
public class DataBaseUtilerias {
    private static String user = "root";
    private static String password = "labsim";
    
    public Connection getConnection(String dataBase){
        Connection con = null;
        String url = "jdbc:mysql://localhost/" + dataBase;
        String urlProps = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        try {
            con = (Connection) DriverManager.getConnection(url + urlProps , user, password);
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return con;
    }
    
    public void createEmpleado(Empleado empleado){
        Statement stmt = null;
        String data =  empleado.getDatatoDB();
        String query = "INSERT INTO MisEmpleados(nombre, apellido, puesto, salario) values (" + data + ");";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(query);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteEmpleado(Empleado empleado){
        Statement stmt = null;
        int data =  empleado.getId();
        String query = "DELETE FROM MisEmpleados WHERE id = "+ data + ";";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(query);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateEmpleadoName(Empleado empleado) {
        Statement stmt = null;
        String query = "UPDATE MisEmpleados SET nombre = '"+ empleado.getNombre()+ "' WHERE id = "+ empleado.getId() + ";";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(query);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateEmpleadoSalario(Empleado empleado) {
        Statement stmt = null;
        String query = "UPDATE MisEmpleados SET salario = '"+ empleado.getSalario()+ "' WHERE id = "+ empleado.getId() + ";";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(query);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public float getEmpleadoSalario(Empleado empleado) {
        float salario = 0;
        Statement stmt = null;
        String query = "SELECT salario FROM MisEmpleados WHERE id = "+empleado.getId()+";";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeQuery(query);
            ResultSet rs = stmt.getResultSet();
            
            
            while(rs.next())
                salario = rs.getInt("salario");
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return salario;
    }

    public ArrayList<Empleado> getEmpleados() {
        ArrayList<Empleado> empleados = new ArrayList();
        String nombre, apellido, puesto;
        int id;
        float salario;
        
        Statement stmt = null;
        String query = "SELECT * FROM MisEmpleados;";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeQuery(query);
            ResultSet rs = stmt.getResultSet();
            
            
            while(rs.next()){
                id = rs.getInt("id");
                nombre = rs.getString("nombre");
                apellido = rs.getString("apellido");
                puesto = rs.getString("puesto");
                salario = rs.getInt("salario");
                
                Empleado empleado = new Empleado(id, nombre, apellido, puesto, salario);
                empleados.add(empleado);
            }
                
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return empleados;
    }

    public float getSalarioPromedio() {
        float salarioProm = 0;
        Statement stmt = null;
        String query = " select SUM(salario)/COUNT(salario) as Promedio FROM MisEmpleados;";
        Connection con = this.getConnection("Empleados");
        
        try {
            stmt = (Statement) con.createStatement();
            stmt.executeQuery(query);
            ResultSet rs = stmt.getResultSet();
            
            
            while(rs.next())
                salarioProm = rs.getInt("Promedio");
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseUtilerias.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return salarioProm;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.Statement;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alumno
 */
public class DbManager {
    private static final String CREA_BASE = "create database IF NOT EXISTS Empleados";
    private static final String DROP_BASE = "drop database Empleados";
    private static final String TABLA_EMPLEADOS = "create table IF NOT EXISTS MisEmpleados (id INT PRIMARY KEY AUTO_INCREMENT, nombre VARCHAR(20), apellido VARCHAR(20), puesto VARCHAR(20), salario FLOAT)";
    private static String user = "root";
    private static String password = "labsim";
    private static ArrayList<String> data = new ArrayList();
    

    private static void getData() {
        String archivo = "empleados.dat";
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                System.out.println("leido "  + linea);
                data.add(linea);
                linea = arch.readLine();
            }
            arch.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void crearDB() {
        Connection con, con2 = null;
        Statement stmt = null;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/";
            String urlProps = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            con = (Connection) DriverManager.getConnection(url + "mysql" +urlProps , user, password);
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(CREA_BASE);
            System.out.println("DBcreada");
            
            con2 = (Connection) DriverManager.getConnection(url + "Empleados"+ urlProps, user, password);
            stmt = (Statement) con2.createStatement();
            stmt.execute(TABLA_EMPLEADOS);
            System.out.println("Tabla Empleados creada");
            
            
            getData();
            
            for(String empleado:data){
                String query = "insert into MisEmpleados(id, nombre, apellido, puesto, salario) values(" + empleado +")";
                stmt.execute(query);
            }
            
            stmt.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void dropDB(){
        Connection con = null;
        Statement stmt = null;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost/";
            String urlProps = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            con = (Connection) DriverManager.getConnection(url + "mysql" +urlProps , user, password);
            stmt = (Statement) con.createStatement();
            stmt.executeUpdate(DROP_BASE);
            System.out.println("DB borrada");
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
}

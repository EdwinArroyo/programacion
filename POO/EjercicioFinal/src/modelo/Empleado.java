/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import db.DataBaseUtilerias;

/**
 *
 * @author Alumno
 */
public class Empleado {
    private int id;
    private String nombre;
    private String apellido;
    private String puesto;
    private float salario;
    private DataBaseUtilerias db = new DataBaseUtilerias();

    public Empleado(String nombre, String apellido, String puesto, float salario) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.puesto = puesto;
        this.salario = salario;
    }

    public Empleado(int id, String nombre, String apellido, String puesto, float salario) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.puesto = puesto;
        this.salario = salario;
    }
    
    public Empleado(int id){
        this.id = id;
    };

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }
    
    public String getDatatoDB(){
        String empleado = "";
        empleado += "'" + this.getNombre() + "', ";
        empleado += "'" + this.getApellido() + "', ";
        empleado += "'" + this.getPuesto()+ "', ";
        empleado += "'" + this.getSalario() + "'";
        
        return empleado;
    }

    public void save() {
        db.createEmpleado(this);
    }
    public void delete(){
        
        db.deleteEmpleado(this);
    }

    public void updateName(String name) {
        this.setNombre(name);
        db.updateEmpleadoName(this);
    }

    public void updateSalario(float newSalario) {
        this.setSalario(newSalario);
        db.updateEmpleadoSalario(this);
    }

    public void getSalarioFromDB() {
       this.salario = db.getEmpleadoSalario(this);
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", puesto=" + puesto + ", salario=" + salario + '}';
    }

    
}

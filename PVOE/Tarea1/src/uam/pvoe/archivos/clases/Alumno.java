/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.archivos.clases;

import java.util.ArrayList;
import uam.pvoe.archivos.operaciones.Escritura;
import uam.pvoe.archivos.operaciones.Lectura;
import uam.pvoe.archivos.operaciones.Separar;

/**
 *
 * @author alumno31
 */
public class Alumno {
    int idAlumno, edad;
    String nombre, primero, segundo;
    String licenciatura;

    public Alumno() {
    }

    public Alumno(int idAlumno, String nombre, String primero, String segundo, String licenciatura, int edad) {
        this.idAlumno = idAlumno;
        this.nombre = nombre;
        this.primero = primero;
        this.segundo = segundo;
        this.licenciatura = licenciatura;
        this.edad = edad;
    }
    

   
    
    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLicenciatura() {
        return licenciatura;
    }

    public void setLicenciatura(String licenciatura) {
        this.licenciatura = licenciatura;
    }

    public int getEdad() {
        return edad;
    }

    @Override
    public String toString() {
        return "Alumno{" + "idAlumno=" + idAlumno + ", edad=" + edad + ", nombre=" + nombre + ", primero=" + primero + ", segundo=" + segundo + ", licenciatura=" + licenciatura + '}';
    }
    

    
    
    
    public void almacenar(ArrayList<Alumno> alumnos, String archivo){
        
        Escritura escribir = new Escritura();
        for(Alumno aux : alumnos){
         escribir.escribir(archivo, aux);   
        }
    }
    public ArrayList<Alumno> leer(){
        ArrayList<Alumno> alumnos = new ArrayList();
        Lectura lec = new Lectura();
        alumnos = lec.leer("Alumnos.txt");
        return alumnos;
    }
    public ArrayList<Alumno> separarComputacion(ArrayList<Alumno> alumnos){
        ArrayList<Alumno> alumnosCompu = new ArrayList();
        Separar separar = new Separar();
        alumnosCompu = separar.separarComputacion(alumnos);
        return alumnosCompu;
    }
    public ArrayList<Alumno> separarEdad(ArrayList<Alumno> alumnos){
        ArrayList<Alumno> alumnosCompu = new ArrayList();
        Separar separar = new Separar();
        alumnosCompu = separar.separarEdad(alumnos);
        return alumnosCompu;
    }
}

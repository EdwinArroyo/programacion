/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.archivos.principal;

import java.util.ArrayList;
import uam.pvoe.archivos.clases.Alumno;

/**
 *
 * @author alumno31
 */
public class Principal {
    public static void main(String[] args) {
//        for(int i = 1; i < 6 ; i++){
//            Alumno alumno = new Alumno(i, "nombre"+ i, "Licenciatura" + i);
//            alumno.almacenar();
//        }
        ArrayList<Alumno> alumnos;
        ArrayList<Alumno> alumnosComputacion;
        ArrayList<Alumno> alumnosMayores;
        Alumno alumnoAux = new Alumno();
        alumnos =  alumnoAux.leer();
        alumnosComputacion = alumnoAux.separarComputacion(alumnos);
        alumnoAux.almacenar(alumnosComputacion, "AlumnosComputacion");
        alumnosMayores = alumnoAux.separarEdad(alumnos);
        alumnoAux.almacenar(alumnosMayores, "AlumnosMayores");
        
        
        //Imprimir Alumnos de Computacion en Consola
        System.out.println("*******  Alumnos de Computacion  *******");
        for(Alumno aux : alumnosComputacion){
            System.out.println(aux);
        }
        
        //Imprimir Alumnos Mayores en Consola
        System.out.println();
        System.out.println("*******  Alumnos Mayores  *******");
        for(Alumno aux : alumnosMayores){
            System.out.println(aux);
        }
    }
}

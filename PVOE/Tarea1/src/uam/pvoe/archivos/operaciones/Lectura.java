/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.archivos.operaciones;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import uam.pvoe.archivos.clases.Alumno;

/**
 *
 * @author alumno31
 */
public class Lectura {
    public ArrayList<Alumno> leer (String nombreArchivo){
        ArrayList<Alumno> listaAlumnos = new ArrayList();
        String cadena = "";
        try {
            FileReader fr = new FileReader(nombreArchivo);
            BufferedReader archivo = new BufferedReader(fr);
            String linea = archivo.readLine();
            while(linea != null){
                StringTokenizer st = new StringTokenizer(linea, ",");
                Alumno alumno = new Alumno(Integer.parseInt(st.nextToken()),st.nextToken(),st.nextToken(),st.nextToken(),st.nextToken(),Integer.parseInt(st.nextToken()));
                listaAlumnos.add(alumno);
                //System.out.println(linea);
                linea = archivo.readLine();
            }
            archivo.close();
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(Lectura.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No lo encontre :'v");
        } catch (IOException ex) {
            //Logger.getLogger(Lectura.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No pude leer </3");
        }
        
        return listaAlumnos;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.archivos.operaciones;
import java.util.ArrayList;
import uam.pvoe.archivos.clases.Alumno;

/**
 *
 * @author e28v1
 */
public class Separar {
   public ArrayList<Alumno> separarComputacion(ArrayList<Alumno> alumnos){
       ArrayList<Alumno> alumnosCompu= new ArrayList();
       for(Alumno aux : alumnos){
           if(aux.getLicenciatura().equals("Computacion")){
               alumnosCompu.add(aux);
           }
       }
       return alumnosCompu;
   }
   public ArrayList<Alumno> separarEdad(ArrayList<Alumno> alumnos){
       ArrayList<Alumno> alumnosMayores= new ArrayList();
       for(Alumno aux : alumnos){
           if(aux.getEdad() >= 19){
               alumnosMayores.add(aux);
           }
       }
       return alumnosMayores;
   }
}

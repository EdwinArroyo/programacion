/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.archivos.operaciones;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import uam.pvoe.archivos.clases.Alumno;

/**
 *
 * @author alumno31
 */
public class Escritura {
    public void escribir(String nombreArchivo, Alumno alumno){
        try{
            FileWriter fw = new FileWriter(nombreArchivo, true);
            BufferedWriter archivoEscritura = new BufferedWriter(fw);
            String almacenar = alumno.toString();
            archivoEscritura.write(almacenar + "\n");
            archivoEscritura.flush();
            archivoEscritura.close();
        } catch(IOException ex){
            System.out.println("No pude :c");
        }
    }
}

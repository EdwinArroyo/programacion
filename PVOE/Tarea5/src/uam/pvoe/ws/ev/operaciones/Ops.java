/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.ws.ev.operaciones;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import uam.pvoe.ws.ev.modelo.Cliente;
import uam.pvoe.ws.ev.modelo.Compra;
import uam.pvoe.ws.ev.modelo.Producto;

/**
 *
 * @author e28v1
 */
public class Ops {

    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> clientes = new ArrayList();
        String archivo = "cliente.dat";
        
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                StringTokenizer tok = new StringTokenizer(linea, ",");
                Cliente cliente = new Cliente(Integer.parseInt(tok.nextToken()), tok.nextToken(), tok.nextToken(), tok.nextToken());
                clientes.add(cliente);
                linea = arch.readLine();
            }
            arch.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return clientes;
    }
    public ArrayList<Compra> getCompras(Cliente cli) {
        ArrayList<Compra> compras = new ArrayList();
        String archivo = "compras_cliente_producto.dat";
        
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                StringTokenizer tok = new StringTokenizer(linea, ",");
                Compra compra = new Compra(Integer.parseInt(tok.nextToken()), Integer.parseInt(tok.nextToken()), Integer.parseInt(tok.nextToken()));
                if(compra.getIntNo_cliente() == cli.getNo_cliente()){
                    compras.add(compra);
                }
                linea = arch.readLine();
            }
            arch.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return compras;
    }
    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> productos = new ArrayList();
        String archivo = "productos.dat";
        
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                StringTokenizer tok = new StringTokenizer(linea, ",");
                Producto producto = new Producto(Integer.parseInt(tok.nextToken()), tok.nextToken());
                productos.add(producto);
                linea = arch.readLine();
            }
            arch.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Ops.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return productos;
    }
}

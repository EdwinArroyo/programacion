 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.ws.ev.modelo;

import java.util.ArrayList;
import uam.pvoe.ws.ev.operaciones.Ops;

/**
 *
 * @author e28v1
 */
public class Cliente {
    Ops ops = new Ops();
    int No_cliente;
    String Nombre, Primer_Apellido,Segundo_Apellido;
    ArrayList<Compra> compras;
    private int totalCompras = 0;
    private int ComprasFaltantes;

    public Cliente(int No_cliente, String Nombre, String Primer_Apellido, String Segundo_Apellido) {
        this.No_cliente = No_cliente;
        this.Nombre = Nombre;
        this.Primer_Apellido = Primer_Apellido;
        this.Segundo_Apellido = Segundo_Apellido;
        this.readCompras();
        this.setComprasFaltantes();
    }

    @Override
    public String toString() {
        return Nombre + " " + Primer_Apellido + " " + Segundo_Apellido;
    }

    public ArrayList getCompras() {
        
        return compras;
    }
    public int getTotalCompras(){
        
        return totalCompras;
    }

    public int getNo_cliente() {
        return No_cliente;
    }
    public int getComprasFaltantes(){
        
        return ComprasFaltantes;
    }
    private void readCompras(){
        this.compras = ops.getCompras(this);
    }

    private void setTotalCompras() {
        for(Compra compra : this.compras){
            this.totalCompras += compra.getCantidad();
        }
    }
    private void setComprasFaltantes() {
        this.setTotalCompras();
        this.ComprasFaltantes = 10-this.totalCompras;
    }

    public String getFullName() {
        return this.Nombre + " " + this.Primer_Apellido + " " + this.Segundo_Apellido;
    }
    
    
    
}

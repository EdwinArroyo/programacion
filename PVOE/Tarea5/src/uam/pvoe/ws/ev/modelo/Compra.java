/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.ws.ev.modelo;

/**
 *
 * @author e28v1
 */
public class Compra {
    private int IntNo_cliente;
    private int No_producto;
    private int Cantidad;
    
    public Compra(int IntNo_cliente, int No_producto, int Cantidad) {
        this.IntNo_cliente = IntNo_cliente;
        this.No_producto = No_producto;
        this.Cantidad = Cantidad;
    }

    public int getIntNo_cliente() {
        return IntNo_cliente;
    }

    public void setIntNo_cliente(int IntNo_cliente) {
        this.IntNo_cliente = IntNo_cliente;
    }

    public int getNo_producto() {
        return No_producto;
    }

    public void setNo_producto(int No_producto) {
        this.No_producto = No_producto;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    @Override
    public String toString() {
        return "Compra{" + "IntNo_cliente=" + IntNo_cliente + ", No_producto=" + No_producto + ", Cantidad=" + Cantidad + '}';
    }
    
}

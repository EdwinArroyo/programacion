/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.ops;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author e28v1
 */
public class IOData {
    public ArrayList<String> read(String archivo){
        ArrayList<String> data = new ArrayList();
        
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader bf = new BufferedReader(fr);
            String linea = bf.readLine();
            while(linea != null){
                data.add(linea);
                linea = bf.readLine();
            }
            bf.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IOData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IOData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return data;
    }
    public void write(String archivo, String line){
        try {
            FileWriter fw = new FileWriter(archivo, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line);
            bw.newLine();
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(IOData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.vista;

/**
 *
 * @author e28v1
 */
public class menu extends javax.swing.JFrame {

    /**
     * Creates new form menu
     */
    public menu() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        btnRegCand = new javax.swing.JButton();
        btnRegEmpr = new javax.swing.JButton();
        btnConsulVacant = new javax.swing.JButton();
        btnBuscCand = new javax.swing.JButton();
        btnReglVacant = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblTitulo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblTitulo.setText("Menu");

        btnRegCand.setText("Registrar candidato");
        btnRegCand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegCandActionPerformed(evt);
            }
        });

        btnRegEmpr.setText("Registrar empresa");
        btnRegEmpr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegEmprActionPerformed(evt);
            }
        });

        btnConsulVacant.setText("Consultar Vacantes");
        btnConsulVacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsulVacantActionPerformed(evt);
            }
        });

        btnBuscCand.setText("Buscar Candidatos");
        btnBuscCand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscCandActionPerformed(evt);
            }
        });

        btnReglVacant.setText("Registrar Vacante");
        btnReglVacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReglVacantActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(397, 397, 397)
                .addComponent(lblTitulo)
                .addContainerGap(436, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnReglVacant, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscCand, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsulVacant, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegEmpr, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegCand, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(lblTitulo)
                .addGap(68, 68, 68)
                .addComponent(btnRegCand)
                .addGap(18, 18, 18)
                .addComponent(btnRegEmpr)
                .addGap(18, 18, 18)
                .addComponent(btnReglVacant)
                .addGap(18, 18, 18)
                .addComponent(btnConsulVacant)
                .addGap(18, 18, 18)
                .addComponent(btnBuscCand)
                .addContainerGap(244, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegEmprActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegEmprActionPerformed
        RegistroEmpresa regemp = new RegistroEmpresa();
        regemp.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnRegEmprActionPerformed

    private void btnRegCandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegCandActionPerformed
        // TODO add your handling code here:
        RegistrarCandidato rc = new RegistrarCandidato();
        rc.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnRegCandActionPerformed

    private void btnReglVacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReglVacantActionPerformed
        // TODO add your handling code here:
        RegistrarVacante rv = new RegistrarVacante();
        rv.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnReglVacantActionPerformed

    private void btnConsulVacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsulVacantActionPerformed
        // TODO add your handling code here:
        ConsultarVacantes cv = new ConsultarVacantes();
        cv.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnConsulVacantActionPerformed

    private void btnBuscCandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscCandActionPerformed
        // TODO add your handling code here:
        BuscarCandidatos bc = new BuscarCandidatos();
        bc.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBuscCandActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscCand;
    private javax.swing.JButton btnConsulVacant;
    private javax.swing.JButton btnRegCand;
    private javax.swing.JButton btnRegEmpr;
    private javax.swing.JButton btnReglVacant;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables
}

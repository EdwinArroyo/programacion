/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.vista;

import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.JOptionPane;
import uam.pvoe.modelo.Empresa;
import uam.pvoe.modelo.Vacante;

/**
 *
 * @author e28v1
 */
public class RegistrarVacante extends javax.swing.JFrame {

    /**
     * Creates new form RegistrarVacante
     */
    public RegistrarVacante() {
        initComponents();
        initForm();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgGenero = new javax.swing.ButtonGroup();
        bgContratacion = new javax.swing.ButtonGroup();
        lblTitulo = new javax.swing.JLabel();
        lblEmpresa = new javax.swing.JLabel();
        cmbEmpresa = new javax.swing.JComboBox<>();
        lblNombre = new javax.swing.JLabel();
        txfNombre = new javax.swing.JTextField();
        lblSueldo = new javax.swing.JLabel();
        txfSueldo = new javax.swing.JTextField();
        lblEdadMin = new javax.swing.JLabel();
        txfEdadMin = new javax.swing.JTextField();
        lblEdadMax = new javax.swing.JLabel();
        txfEdadMax = new javax.swing.JTextField();
        lblExp = new javax.swing.JLabel();
        txfExp = new javax.swing.JTextField();
        btnRegistrar = new javax.swing.JButton();
        lblHoras = new javax.swing.JLabel();
        txfHoras = new javax.swing.JTextField();
        lblGenero = new javax.swing.JLabel();
        rbGenHombre = new javax.swing.JRadioButton();
        rbGenMujer = new javax.swing.JRadioButton();
        lblContratacion = new javax.swing.JLabel();
        rbContDef = new javax.swing.JRadioButton();
        rbContTemp = new javax.swing.JRadioButton();
        lblContTempTiempo = new javax.swing.JLabel();
        txfContTempTiempo = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblTitulo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTitulo.setText("Registro Vacante");

        lblEmpresa.setText("Empresa");

        cmbEmpresa.setModel(new javax.swing.DefaultComboBoxModel<>());

        lblNombre.setText("Nombre del Puesto");

        txfNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfNombreActionPerformed(evt);
            }
        });

        lblSueldo.setText("Sueldo");

        lblEdadMin.setText("Edad Minima");

        lblEdadMax.setText("Edad Maxima");

        lblExp.setText("Años Experiencia");

        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        lblHoras.setText("Horas al dia");

        txfHoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfHorasActionPerformed(evt);
            }
        });

        lblGenero.setText("Genero");

        rbGenHombre.setText("Hombre");

        rbGenMujer.setText("Mujer");

        lblContratacion.setText("Tipo de Contratacion");

        rbContDef.setText("Definitiva");
        rbContDef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbContDefActionPerformed(evt);
            }
        });

        rbContTemp.setText("Temporal");
        rbContTemp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbContTempActionPerformed(evt);
            }
        });

        lblContTempTiempo.setText("Tiempo de Contratacion");

        jButton1.setText("Regresar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(395, 395, 395)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btnRegistrar)
                            .addComponent(lblTitulo)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblEmpresa)
                                    .addComponent(cmbEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(114, 114, 114)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNombre)
                                    .addComponent(txfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txfSueldo, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(lblSueldo))
                                                .addGap(114, 114, 114)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblEdadMin)
                                                    .addComponent(txfEdadMin, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 122, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblExp)
                                                    .addComponent(txfExp, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblHoras)
                                                    .addComponent(txfHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(154, 154, 154)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblEdadMax)
                                            .addComponent(txfEdadMax, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblGenero)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(rbGenHombre)
                                                .addGap(57, 57, 57)
                                                .addComponent(rbGenMujer))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(101, 101, 101)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblContratacion)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(rbContDef)
                                                .addGap(57, 57, 57)
                                                .addComponent(rbContTemp)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblContTempTiempo)
                                            .addComponent(txfContTempTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(197, 197, 197)))))))
                .addGap(51, 51, 51))
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(lblTitulo)
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblEmpresa)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cmbEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblNombre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(38, 38, 38)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblSueldo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txfSueldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(lblEdadMin)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txfEdadMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblEdadMax)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txfEdadMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblExp)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txfExp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblGenero)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rbGenHombre)
                                    .addComponent(rbGenMujer)))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblHoras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txfHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblContratacion)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbContDef)
                            .addComponent(rbContTemp)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblContTempTiempo)
                        .addGap(18, 18, 18)
                        .addComponent(txfContTempTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                .addComponent(btnRegistrar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txfNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfNombreActionPerformed

    private void txfHorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfHorasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfHorasActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        menu menu = new menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void rbContDefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbContDefActionPerformed
        // TODO add your handling code here:
        this.lblContTempTiempo.setVisible(false);
        this.txfContTempTiempo.setVisible(false);
    }//GEN-LAST:event_rbContDefActionPerformed

    private void rbContTempActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbContTempActionPerformed
        // TODO add your handling code here:
        this.lblContTempTiempo.setVisible(true);
        this.txfContTempTiempo.setVisible(true);
    }//GEN-LAST:event_rbContTempActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        // TODO add your handling code here:
        registar();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarVacante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarVacante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarVacante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarVacante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistrarVacante().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgContratacion;
    private javax.swing.ButtonGroup bgGenero;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JComboBox<Empresa> cmbEmpresa;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel lblContTempTiempo;
    private javax.swing.JLabel lblContratacion;
    private javax.swing.JLabel lblEdadMax;
    private javax.swing.JLabel lblEdadMin;
    private javax.swing.JLabel lblEmpresa;
    private javax.swing.JLabel lblExp;
    private javax.swing.JLabel lblGenero;
    private javax.swing.JLabel lblHoras;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblSueldo;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JRadioButton rbContDef;
    private javax.swing.JRadioButton rbContTemp;
    private javax.swing.JRadioButton rbGenHombre;
    private javax.swing.JRadioButton rbGenMujer;
    private javax.swing.JTextField txfContTempTiempo;
    private javax.swing.JTextField txfEdadMax;
    private javax.swing.JTextField txfEdadMin;
    private javax.swing.JTextField txfExp;
    private javax.swing.JTextField txfHoras;
    private javax.swing.JTextField txfNombre;
    private javax.swing.JTextField txfSueldo;
    // End of variables declaration//GEN-END:variables

    private void initForm() {
        agruparRB();
        initCBX();
        this.lblContTempTiempo.setVisible(false);
        this.txfContTempTiempo.setVisible(false);
    }

    private void agruparRB() {
        this.bgGenero.add(this.rbGenHombre);
        this.bgGenero.add(this.rbGenMujer);
        this.rbGenHombre.setSelected(true);
        
        this.bgContratacion.add(this.rbContDef);
        this.bgContratacion.add(this.rbContTemp);
        this.rbContDef.setSelected(true);
    }

    private void initCBX() {
        this.cmbEmpresa.removeAllItems();
        Empresa emp = new Empresa();
        ArrayList<Empresa> empresas = emp.getEmpresas();
        for (Empresa empresa : empresas){
            this.cmbEmpresa.addItem(empresa);
        }
    }

    private void registar() {
        String name = this.txfNombre.getText();
        String minage = this.txfEdadMin.getText();
        String maxage = this.txfEdadMax.getText();
        String exp = this.txfExp.getText();
        String hours = this.txfHoras.getText();
        String gender = getSelectedGender();
        String hiring = getSelectedHiring();
        String hiringTime = this.txfContTempTiempo.getText();
        String company = getSelectedCompany();
        String sueldo = this.txfSueldo.getText();
        
        Vacante vacante = new Vacante(name, minage, maxage, exp, hours, gender, hiring, hiringTime, company, sueldo);
        vacante.save();
        
        JOptionPane.showMessageDialog(this,"Vacante Registrada");
        reset();
    }

    private String getSelectedCompany() {
        Empresa comp = (Empresa) this.cmbEmpresa.getSelectedItem();
        return comp.getRFC();
    }

    private String getSelectedGender() {
        String gender;
        if(this.rbGenHombre.isSelected()){
            gender = "Hombre";
        } else {
            gender = "Mujer";
        }
        
        return gender;
    }

    private String getSelectedHiring() {
         String hiring;
        if(this.rbContDef.isSelected()){
            hiring = "Definitiva";
            this.txfContTempTiempo.setText("0");
        } else {
            hiring = "Temporal";
        }
        
        return hiring;
    }

    private void reset() {
        this.txfNombre.setText("");
        this.txfEdadMin.setText("");
        this.txfEdadMax.setText("");
        this.txfExp.setText("");
        this.txfHoras.setText("");
        this.txfContTempTiempo.setText("");
        this.txfSueldo.setText("");
        this.rbContDef.setSelected(true);
        this.rbGenHombre.setSelected(true);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.modelo;

import java.util.ArrayList;
import java.util.StringTokenizer;
import uam.pvoe.ops.IOData;

/**
 *
 * @author e28v1
 */
public class Empresa {
    private String name;
    private String RFC;
    private int state;
    private String addres;
    private String phone;
    private String giro;
    private static String archivo = "Empresas.dat";

    public Empresa(String name, String RFC, int state, String addres, String phone, String giro) {
        this.name = name;
        this.RFC = RFC;
        this.state = state;
        this.addres = addres;
        this.phone = phone;
        this.giro = giro;
    }

    public Empresa() {
        
    }

    public String getRFC() {
        return RFC;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }
    
    public void save() {
        IOData io = new IOData();
        String line;
        line = name + "," + RFC + "," + state + "," + addres + "," + phone + "," + giro;
        io.write(archivo, line);
    }
    public ArrayList<Empresa> getEmpresas(){
        ArrayList empresas = new ArrayList();
        IOData io = new IOData();
        ArrayList<String> lineas = io.read(archivo);
        for(String linea: lineas){
            StringTokenizer tok = new StringTokenizer(linea, ",");
            Empresa empresa = new Empresa(tok.nextToken(), tok.nextToken(), Integer.parseInt(tok.nextToken()), tok.nextToken(), tok.nextToken(), tok.nextToken());
            empresas.add(empresa);
        }
        
        return empresas;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.modelo;

import java.util.ArrayList;
import java.util.StringTokenizer;
import uam.pvoe.ops.IOData;

/**
 *
 * @author e28v1
 */
public class Estado {
    private int id;
    private String name;
    private String abbr;
    
    private static String archivo = "Estados.dat";

    public Estado(int id, String name, String abbr) {
        this.id = id;
        this.name = name;
        this.abbr = abbr;
    }
    public Estado(){
        
    }

    public int getId() {
        return id;
    }
    
    public ArrayList<Estado> getEstados(){
        ArrayList estados = new ArrayList();
        IOData io = new IOData();
        ArrayList<String> lineas = io.read(this.archivo);
        for(String linea : lineas){
            StringTokenizer tok = new StringTokenizer(linea, ",");
            Estado estado = new Estado(Integer.parseInt(tok.nextToken()), tok.nextToken(), tok.nextToken());
            estados.add(estado);
        }
        
        return estados;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}

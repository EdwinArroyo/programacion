/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.modelo;

import java.util.ArrayList;
import java.util.StringTokenizer;
import uam.pvoe.ops.IOData;

/**
 *
 * @author e28v1
 */
public class User {
    String user;
    String password;
    Boolean logged;
    private String archivo = "Users.dat";

    public User(String user, String password) {
        this();
        this.user = user;
        this.password = password;
    }

    public User() {
        this.logged = false;
    }
    

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }

    public Boolean login(){
        IOData io = new IOData();
        ArrayList<String> users = io.read(this.archivo);
        for(String user:users){
            StringTokenizer tok = new StringTokenizer(user, ",");
            if(this.getUser().equals(tok.nextToken()) && this.getPassword().equals(tok.nextToken())){
                this.setLogged(true);
            }
        }
        return this.logged;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.modelo;

import java.util.ArrayList;
import java.util.StringTokenizer;
import uam.pvoe.ops.IOData;

/**
 *
 * @author e28v1
 */
public class Vacante {
    private String name;
    private String minage;
    private String maxage;
    private String exp;
    private String hours;
    private String gender;
    private String hiring;
    private String hiringTime;
    private String company;
    private String sueldo;
    private static String archivo = "Vacantes.dat";
    
    public Vacante(String name, String minage, String maxage, String exp, String hours, String gender, String hiring, String hiringTime, String company, String sueldo) {
        this.name = name;
        this.minage = minage;
        this.maxage = maxage;
        this.exp = exp;
        this.hours = hours;
        this.gender = gender;
        this.hiring = hiring;
        this.hiringTime = hiringTime;
        this.company = company;
        this.sueldo = sueldo;
    }

    public Vacante() {
        
    }

    public String getName() {
        return name;
    }

    public String getHours() {
        return hours;
    }

    public String getHiring() {
        return hiring;
    }

    public String getSueldo() {
        return sueldo;
    }

    public String getCompany() {
        return company;
    }

    
    
    
    public void save() {
        IOData io = new IOData();
        String line;
        
        line = name + "," + minage + "," + maxage + "," + exp + "," + hours + "," + gender + "," + hiring + "," + hiringTime + "," + company + "," + sueldo;
        
        io.write(archivo, line);
    }

    public ArrayList<Vacante> getVacantes(String rfc) {
        ArrayList<Vacante> vacantes = new ArrayList();
        IOData io = new IOData();
        ArrayList<String> lineas = io.read(archivo);
        for(String linea : lineas){
            StringTokenizer tok = new StringTokenizer(linea, ",");
            Vacante vacante = new Vacante(tok.nextToken(),tok.nextToken(),tok.nextToken(),
                                          tok.nextToken(),tok.nextToken(),tok.nextToken(),
                                          tok.nextToken(),tok.nextToken(),tok.nextToken(),tok.nextToken());
            if(vacante.getCompany().equals(rfc)){
                vacantes.add(vacante);
            }
        }
        
        return vacantes;
    }
    
    
    
   
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.tarea4.operaciones;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import uam.pvoe.tarea4.modelo.Atractivo;
import uam.pvoe.tarea4.modelo.Estado;
import uam.pvoe.tarea4.modelo.MedioTransporte;
import uam.pvoe.tarea4.modelo.Servicio;

/**
 *
 * @author josue
 */
public class Operaciones {
    
    /*Cargar del archivo los estados*/
    public ArrayList<Estado>llenarListaEstados(){
        ArrayList<Estado> listaEstados = new ArrayList();
        String archivo = "estados.dat";
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                StringTokenizer tok = new StringTokenizer(linea, ",");
                Estado estado = new Estado(tok.nextToken(), tok.nextToken());
                listaEstados.add(estado);
                linea = arch.readLine();
            }
            arch.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaEstados;
    }
    
    
    /*Cargar del archivo los atractivos de un estado*/
    public ArrayList<Atractivo>llenarListaAtractivos(String claveEstado){
        ArrayList<Atractivo> listaAtractivos = new ArrayList();
        String archivo = "atractivos.dat";
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader arch = new BufferedReader(fr);
            String linea = arch.readLine();
            while(linea != null){
                StringTokenizer tok = new StringTokenizer(linea, ",");
                Atractivo atractivo = new Atractivo(tok.nextToken(), tok.nextToken());
                if(tok.nextToken().equalsIgnoreCase(claveEstado)){
                    listaAtractivos.add(atractivo);
                }
                linea = arch.readLine();
            }
            arch.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaAtractivos;
    }
    
    public ArrayList<MedioTransporte> llenarListaMediosTransporte(){
        ArrayList<MedioTransporte> listaMedios = new ArrayList();
        String[] claves = { "AUTO", "BUS", "AVION", "AUTO_R"};
        String[] nombres = {"Auto", "Autobús", "Avión", "Auto Rentado"};
        for(int i = 0; i < claves.length; i++){
            MedioTransporte medtr = new MedioTransporte(nombres[i],claves[i]);
            listaMedios.add(medtr);
        }
        return listaMedios;
    }
    public ArrayList<Servicio> llenarListaServicios(){
        ArrayList<Servicio> listaServicios = new ArrayList();
        String[] claves = { "DES", "REC", "LAV", "GUAR", "CAJA", "DESP"};
        String[] nombres = {"Desayuno", "Recorridos", "Lavandería", "Guardería", "Caja Fuerte","Despertador"};
        for(int i = 0; i < claves.length; i++){
            Servicio servicio = new Servicio(nombres[i],claves[i]);
            listaServicios.add(servicio);
        }
        return listaServicios;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uam.pvoe.tarea4.modelo;

/**
 *
 * @author josue
 */
public class Atractivo {
    
    private String nombre;
    private String atractivo;

    public Atractivo(String nombre, String atractivo) {
        this.nombre = nombre;
        this.atractivo = atractivo;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAtractivo() {
        return atractivo;
    }

    public void setAtractivo(String atractivo) {
        this.atractivo = atractivo;
    }

    @Override
    public String toString() {
        return atractivo;
    }
    
    
    
    
}
